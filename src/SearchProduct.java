import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.Scanner;

public class SearchProduct {

    // main search product method
    static void searchProduct(){
        ShowTable t = new ShowTable();
        int count = 0;

        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Search by Name : ");
            String s = sc.nextLine();
            Connection c = Connect_to_DB.connectDB();
            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM stock_management WHERE name LIKE ?");
            preparedStatement.setString(1,  s + '%');
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                int idd = rs.getInt("id");
                String name = rs.getString("name");
                float unit_price = rs.getFloat("unit_price");
                int qty = rs.getInt("qty");
                Date datee = rs.getDate("import_date");
                t.table(idd,name,unit_price,qty,datee);
                count++;
            }
            if (count != 0){
                t.renderTable();
                System.out.println(" Total Record : " + count);
            }else {
                System.out.println();
                System.out.println("    Product not exist");
                System.out.println();
            }
            c.close();
            preparedStatement.close();
            rs.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
