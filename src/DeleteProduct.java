import java.sql.*;
import java.util.Date;
import java.util.Scanner;

public class DeleteProduct {
    int r_id;
    String r_name;
    float r_unit_price;
    int r_qty;
    Date r_date;

    int deleteID;
    boolean isdelete;

    boolean isDelete() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Are you sure you want to delete this record? [Y/y] or [N/n]: ");
        String s = sc.nextLine();
        if (s.equals("N") || s.equals("n")) {
            isdelete = false;
        } else if (s.equals("Y") || s.equals("y")) {
            isdelete = true;
        } else {
            isDelete();
        }
        return isdelete;
    }

    public int getDeleteID() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please Input ID of Product : ");
        String s = sc.nextLine();
        if (Validate.regexNumber(s) == false) {
            getDeleteID();
        } else {
            deleteID = Integer.parseInt(s);
        }
        return deleteID;
    }


//insert deleted record to recovery table
    public void insertRecovery(int id, String name, float unit_price, int qty, Date date) {
        try {
            Connection c = Connect_to_DB.connectDB();
            PreparedStatement preparedStatement = c.prepareStatement("INSERT INTO recovery VALUES(?,?,?,?,?)");
            preparedStatement.setInt(1, id); //increase id, by yourself
            preparedStatement.setString(2, name);
            preparedStatement.setFloat(3, unit_price);
            preparedStatement.setInt(4, qty);
            preparedStatement.setDate(5, (java.sql.Date) date);
            preparedStatement.executeUpdate();
            c.close();
            preparedStatement.close();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    // delete record from recovery by id
    public static void deleteFromRecovery(int id) {
        try {
            Connection c = Connect_to_DB.connectDB();
            PreparedStatement preparedStatement = c.prepareStatement("DELETE FROM recovery WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            c.close();
            preparedStatement.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    public static void deleteAllstock(){
        try {
            Connection c = Connect_to_DB.connectDB();
            Statement stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM stock_management");
            c.close();
            stmt.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    // delete all record from recovery table need to use this in save function
    public static void deleteAllRecovery() {
        try {
            Connection c = Connect_to_DB.connectDB();
            Statement stmt = c.createStatement();
            stmt.executeUpdate("DELETE FROM recovery");
            c.close();
            stmt.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
// main delete product use to delete record from stock management table and insert deleted record to recovery
    public void deleteProduct() {
        int count = 0;
        int id = getDeleteID();
        try {
            Connection c = Connect_to_DB.connectDB();
            PreparedStatement preparedStatement = c.prepareStatement("SELECT * FROM stock_management WHERE id = ?");
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int idd = rs.getInt("id");
                String name = rs.getString("name");
                float unit_price = rs.getFloat("unit_price");
                int qty = rs.getInt("qty");
                Date datee = rs.getDate("import_date");
                System.out.println();
                ShowTable.box(idd, name, unit_price, qty, datee);
                System.out.println();
                count++;
                r_id = id;
                r_name = name;
                r_date = datee;
                r_qty = qty;
                r_unit_price = unit_price;
            }
            if (count == 0) {
                System.out.println();
                System.out.println("Product with ID : " + id + " Not exist");
                System.out.println();
            } else {
                boolean isdeleting = isDelete();
                if (isdeleting == true) {
                    PreparedStatement preparedDelete = c.prepareStatement("DELETE FROM stock_management WHERE id = ?");
                    preparedDelete.setInt(1, id);
                    preparedDelete.executeUpdate();
                    preparedDelete.close();
                    System.out.println();
                    System.out.println("------------------------");
                    System.out.println("   Product was Remove");
                    System.out.println("------------------------");
                    System.out.println();

                    insertRecovery(r_id, r_name, r_unit_price, r_qty, r_date);


                } else {
                    System.out.println();
                    System.out.println("Delete CANCELED");
                    System.out.println();
                }
            }

            rs.close();
            preparedStatement.close();
            c.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
